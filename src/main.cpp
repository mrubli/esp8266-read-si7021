/*
 * Si7021 Temperature/humidity sensor read-out
 *
 * Pin assignment:
 * Si7021   NodeMCU
 * SCL      D1
 * SDA      D2
 */

#include <Arduino.h>
#include <Wire.h>


#define PRINT_RAW_DATA 0


namespace
{

    const auto I2cAddress = 0x40;

}


void setup()
{
    Serial.begin(9600);

#if 1
    // Initialize I2C with default pin parameters
    Wire.begin();   // Pins 4 (D2 = SDA) and 5 (D1 = SCL)
#else
    // Use this for custom setups (these values are for the NodeMCU board)
    static_assert(D1 == 5, "D1 ouch");
    static_assert(D2 == 4, "D2 ouch");
    static_assert(SDA == D2, "SDA ouch");
    static_assert(SCL == D1, "SCL ouch");
    Wire.begin(SDA, SCL);
#endif
}

void loop()
{
    delay(1000);

    // Start a read for the relative humidity
    {
        Wire.beginTransmission(I2cAddress);
        byte outBuffer[1] = {
            0xE5    // Measure Relative Humidity, Hold Master Mode
        };
        Wire.write(outBuffer, sizeof(outBuffer));
        const auto res = Wire.endTransmission();
        if(res != 0)
        {
            Serial.print("ERROR: I2C write failed: ");
            Serial.print(res);
            Serial.println();
            return;
        }
    }

    // Read the response data
    {
        byte inBuffer[3] = {};

        Wire.requestFrom(I2cAddress, 3);
        auto offset = 0u;
#if PRINT_RAW_DATA
        Serial.print("Received:");
#endif
        while(offset < sizeof(inBuffer) && Wire.available() > 0)
        {
            inBuffer[offset] = Wire.read();
#if PRINT_RAW_DATA
            Serial.print(" 0x");
            Serial.print(inBuffer[offset], HEX);
#endif
            offset++;
        }
#if PRINT_RAW_DATA
        Serial.println();
#endif
        if(offset < sizeof(inBuffer))
        {
            Serial.print("ERROR: Short I2C read (");
            Serial.print(offset);
            Serial.println(" bytes, expected: 3 bytes)");
            return;
        }

        const auto rh = 125 * ((static_cast<uint16_t>(inBuffer[0]) << 8) | inBuffer[1]) / 65536.0f - 6;
        Serial.print("%RH:  ");
        Serial.print(rh);
        Serial.println();
    }

    // Start a read for the associated temperature
    {
        Wire.beginTransmission(I2cAddress);
        byte outBuffer[1] = {
            0xE0    // Read Temperature Value from Previous RH Measurement
        };
        Wire.write(outBuffer, sizeof(outBuffer));
        const auto res = Wire.endTransmission();
        if(res != 0)
        {
            Serial.print("ERROR: I2C write failed: ");
            Serial.print(res);
            Serial.println();
            return;
        }
    }

    // Read the response data
    {
        byte inBuffer[3] = {};

        Wire.requestFrom(I2cAddress, 3);
        auto offset = 0u;
#if PRINT_RAW_DATA
        Serial.print("Received:");
#endif
        while(offset < sizeof(inBuffer) && Wire.available() > 0)
        {
            inBuffer[offset] = Wire.read();
#if PRINT_RAW_DATA
            Serial.print(" 0x");
            Serial.print(inBuffer[offset], HEX);
#endif
            offset++;
        }
#if PRINT_RAW_DATA
        Serial.println();
#endif
        if(offset < sizeof(inBuffer))
        {
            Serial.print("ERROR: Short I2C read (");
            Serial.print(offset);
            Serial.println(" bytes, expected: 3 bytes)");
            return;
        }

        const auto temp = 175.72f * ((static_cast<uint16_t>(inBuffer[0]) << 8) | inBuffer[1]) / 65536.0f - 48.85f;
        Serial.print("Temp: ");
        Serial.print(temp);
        Serial.println(" °C");
    }
}
